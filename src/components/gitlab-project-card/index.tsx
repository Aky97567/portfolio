import { Fragment, useEffect, useState } from 'react';
import { AiOutlineFork, AiOutlineStar } from 'react-icons/ai';
import { MdInsertLink } from 'react-icons/md';
import { ga, getLanguageColor, skeleton } from '../../utils';
import { GitlabProject } from '../../interfaces/gitlab-project';
import axios from 'axios';

type ProjectLanguages = {
  [language: string]: number;
};

type SanitizedGitlabProject = GitlabProject & {
  language?: string;
  languages?: ProjectLanguages;
};

const GitlabProjectCard = ({
  header,
  gitlabProjects,
  loading,
  limit,
  username,
  googleAnalyticsId,
  showStarred,
}: {
  header: string;
  gitlabProjects: GitlabProject[];
  loading: boolean;
  limit: number;
  username: string;
  googleAnalyticsId?: string;
  showStarred: boolean;
}) => {
  const [projects, setProjects] = useState<SanitizedGitlabProject[]>([]);

  useEffect(() => {
    const fetchProjects = async () => {
      const projectsData = await Promise.all(
        gitlabProjects.map(async (project) => {
          try {
            const response = await axios.get(
              `https://gitlab.com/api/v4/projects/${project.id}/languages`,
              { headers: { 'Content-Type': 'application/json' } },
            );
            const languages = response.data as ProjectLanguages;
            const maxLanguage = Object.keys(languages).reduce((a, b) =>
              languages[a] > languages[b] ? a : b,
            );
            return { ...project, language: maxLanguage, languages };
          } catch (error) {
            console.error(
              `Error fetching languages for project ID ${project.id}`,
              error,
            );
            return project;
          }
        }),
      );
      setProjects(projectsData);
    };

    if (gitlabProjects.length > 0) {
      fetchProjects();
    }
  }, [gitlabProjects]);

  const renderSkeleton = () => {
    const array = [];
    for (let index = 0; index < limit; index++) {
      array.push(
        <div className="card shadow-lg compact bg-base-100" key={index}>
          <div className="flex justify-between flex-col p-8 h-full w-full">
            <div>
              <div className="flex items-center">
                <span>
                  <h5 className="card-title text-lg">
                    {skeleton({
                      widthCls: 'w-32',
                      heightCls: 'h-8',
                      className: 'mb-1',
                    })}
                  </h5>
                </span>
              </div>
              <div className="mb-5 mt-1">
                {skeleton({
                  widthCls: 'w-full',
                  heightCls: 'h-4',
                  className: 'mb-2',
                })}
                {skeleton({ widthCls: 'w-full', heightCls: 'h-4' })}
              </div>
            </div>
            <div className="flex justify-between">
              <div className="flex flex-grow">
                <span className="mr-3 flex items-center">
                  {skeleton({ widthCls: 'w-12', heightCls: 'h-4' })}
                </span>
                <span className="flex items-center">
                  {skeleton({ widthCls: 'w-12', heightCls: 'h-4' })}
                </span>
              </div>
              <div>
                <span className="flex items-center">
                  {skeleton({ widthCls: 'w-12', heightCls: 'h-4' })}
                </span>
              </div>
            </div>
          </div>
        </div>,
      );
    }

    return array;
  };

  const renderProjects = () => {
    return projects.slice(0, limit).map((item, index) => (
      <a
        className="card shadow-lg compact bg-base-100 cursor-pointer"
        href={item.avatar_url}
        key={index}
        onClick={(e) => {
          e.preventDefault();

          try {
            if (googleAnalyticsId) {
              ga.event('Click project', {
                project: item.name,
              });
            }
          } catch (error) {
            console.error(error);
          }

          window?.open(item.web_url, '_blank');
        }}
      >
        <div className="flex justify-between flex-col p-8 h-full w-full">
          <div>
            <div className="flex items-center truncate">
              <div className="card-title text-lg tracking-wide flex text-base-content opacity-60">
                <MdInsertLink className="my-auto" />
                <span>{item.name}</span>
              </div>
            </div>
            <p className="mb-5 mt-1 text-base-content text-opacity-60 text-sm">
              {item.description}
            </p>
          </div>
          <div className="flex justify-between text-sm text-base-content text-opacity-60 truncate">
            <div className="flex flex-grow">
              <span className="mr-3 flex items-center">
                <AiOutlineStar className="mr-0.5" />
                <span>{item.star_count}</span>
              </span>
              <span className="flex items-center">
                <AiOutlineFork className="mr-0.5" />
                <span>{item.forks_count}</span>
              </span>
            </div>
            <div>
              {item.language && (
                <span className="flex items-center">
                  <div
                    className="w-3 h-3 rounded-full mr-1 opacity-60"
                    style={{ backgroundColor: getLanguageColor(item.language) }}
                  />
                  <span>{item.language}</span>
                </span>
              )}
            </div>
          </div>
        </div>
      </a>
    ));
  };

  return gitlabProjects.length === 0 ? null : (
    <Fragment>
      <div className="col-span-1 lg:col-span-2">
        <div className="grid grid-cols-2 gap-6">
          <div className="col-span-2">
            <div className="card compact bg-base-100 shadow bg-opacity-40">
              <div className="card-body">
                <div className="mx-3 flex items-center justify-between mb-2">
                  <h5 className="card-title">
                    {loading ? (
                      skeleton({ widthCls: 'w-40', heightCls: 'h-8' })
                    ) : (
                      <span className="text-base-content opacity-70">
                        {header}
                      </span>
                    )}
                  </h5>
                  {loading ? (
                    skeleton({ widthCls: 'w-10', heightCls: 'h-5' })
                  ) : (
                    <a
                      href={`https://gitlab.com/users/${username}/${showStarred ? 'starred' : 'projects'}`}
                      target="_blank"
                      rel="noreferrer"
                      className="text-base-content opacity-50 hover:underline"
                    >
                      See All
                    </a>
                  )}
                </div>
                <div className="col-span-2">
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
                    {loading ? renderSkeleton() : renderProjects()}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default GitlabProjectCard;
