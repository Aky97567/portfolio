// gitprofile.config.ts

const CONFIG = {
  github: {
    username: 'aky97567', // Your GitHub org/user name. (This is the only required config)
  },
  gitlab: {
    username: 'aky97567', // Your Gitlab org/user name. (This is the only required config)
  },
  base: '/',
  projects: {
    github: {
      display: true, // Display GitHub projects?
      header: 'Recent musings on Github',
      mode: 'automatic', // Mode can be: 'automatic' or 'manual'
      automatic: {
        sortBy: 'updated', // Sort projects by 'stars' or 'updated'
        limit: 2, // How many projects to display.
        exclude: {
          forks: true, // Forked projects will not be displayed if set to true.
          projects: [], // These projects will not be displayed. example: ['aky97567/my-project1', 'aky97567/my-project2']
        },
      },
      manual: {
        // Properties for manually specifying projects
        projects: ['aky97567/gitprofile', 'aky97567/pandora'], // List of repository names to display. example: ['aky97567/my-project1', 'aky97567/my-project2']
      },
    },
    gitlab: {
      display: true, // Display Gitlab projects?
      header: 'Recent musings on Gitlab',
      showStarred: true, // show projects or show starred projects under gilab username
      automatic: {
        sortBy: 'stars', // Sort projects by 'stars' or 'updated'
        limit: 4, // How many projects to display.
        exclude: {
          forks: false, // Forked projects will not be displayed if set to true.
          projects: [], // These projects will not be displayed. example: ['aky97567/my-project1', 'aky97567/my-project2']
        },
      },
      manual: {
        // Properties for manually specifying projects
        projects: ['aky97567/gitprofile', 'aky97567/pandora'], // List of repository names to display. example: ['aky97567/my-project1', 'aky97567/my-project2']
      },
    },
    external: {
      header: 'Recent Musings on Gitlab',
      // To hide the `External Projects` section, keep it empty.
      projects: [],
    },
  },
  seo: {
    title: 'Portfolio of Akshay Choudhry',
    description: '',
    imageURL: '',
  },
  social: {
    linkedin: 'aky97567',
    twitter: 'aky97567',
    mastodon: 'aky97567@mastodon.social',
    researchGate: '',
    facebook: '',
    instagram: '',
    youtube: '', // example: 'pewdiepie'
    dribbble: '',
    behance: '',
    medium: '',
    dev: '',
    stackoverflow: '', // example: '1/jeff-atwood'
    skype: '',
    telegram: '',
    website: '',
    phone: '',
    email: 'mail@akshayc.dev',
  },
  resume: {
    fileUrl:
      'https://onedrive.live.com/redir?resid=92D70C0BBED8B922!10967&authkey=!ADcuChRl1st3Dt8&ithint=file%2cpdf&e=GdoiXs', // Empty fileUrl will hide the `Download Resume` button.
  },
  skills: [
    'TypeScript',
    'NextJS',
    'JavaScript',
    'React.js',
    'Git',
    'Docker',
    'Jest',
    'CSS',
    'Tailwind',
    'Emotion',
  ],
  experiences: [
    {
      company: 'Floyt Mobility GmbH',
      position: 'Frontend Developer',
      from: 'January 2023',
      to: 'Present',
    },
    {
      company: 'Favendo GmbH',
      position: 'Software Developer',
      from: 'April 2021',
      to: 'December 2022',
    },
    {
      company: 'Favendo GmbH',
      position: 'Junior Software Developer',
      from: 'September 2019',
      to: 'March 2021',
    },
    {
      company: 'Mahindra Comviva',
      position: 'Software Engineer',
      from: 'August 2016',
      to: 'August 2018',
    },
  ],
  certifications: [],
  educations: [
    {
      institution: 'Otto Friedrich Universität, Bamberg, Deutschland',
      degree: 'M.Sc. Internation Software Systems Science',
      from: '2018',
      to: '2021',
    },
    {
      institution: 'Manipal Institute of Technology, Manipal, India',
      degree: 'B.Tech. Information Technology',
      from: '2012',
      to: '2016',
    },
  ],
  publications: [],
  // Display articles from your medium or dev account. (Optional)
  blog: {
    source: 'dev', // medium | dev
    username: '', // to hide blog section, keep it empty
    limit: 2, // How many articles to display. Max is 10.
  },
  googleAnalytics: {
    id: '', // GA3 tracking id/GA4 tag id UA-XXXXXXXXX-X | G-XXXXXXXXXX
  },
  // Track visitor interaction and behavior. https://www.hotjar.com
  hotjar: {
    id: '',
    snippetVersion: 6,
  },
  themeConfig: {
    defaultTheme: 'fantasy',

    // Hides the switch in the navbar
    // Useful if you want to support a single color mode
    disableSwitch: true,

    // Should use the prefers-color-scheme media-query,
    // using user system preferences, instead of the hardcoded defaultTheme
    respectPrefersColorScheme: false,

    // Display the ring in Profile picture
    displayAvatarRing: false,

    // Available themes. To remove any theme, exclude from here.
    themes: [
      'light',
      'dark',
      'cupcake',
      'bumblebee',
      'emerald',
      'corporate',
      'synthwave',
      'retro',
      'cyberpunk',
      'valentine',
      'halloween',
      'garden',
      'forest',
      'aqua',
      'lofi',
      'pastel',
      'fantasy',
      'wireframe',
      'black',
      'luxury',
      'dracula',
      'cmyk',
      'autumn',
      'business',
      'acid',
      'lemonade',
      'night',
      'coffee',
      'winter',
      'dim',
      'nord',
      'sunset',
      'procyon',
    ],

    // Custom theme, applied to `procyon` theme
    customTheme: {
      primary: '#fc055b',
      secondary: '#219aaf',
      accent: '#e8d03a',
      neutral: '#2A2730',
      'base-100': '#E3E3ED',
      '--rounded-box': '3rem',
      '--rounded-btn': '3rem',
    },
  },

  // Optional Footer. Supports plain text or HTML.
  footer: `Made with <a 
  class="text-primary" href="https://github.com/arifszn/gitprofile"
  target="_blank"
  rel="noreferrer"
>GitProfile</a> and ❤️`,

  enablePWA: true,
};

export default CONFIG;
