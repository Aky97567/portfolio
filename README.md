# PORTFOLIO-APP

This is the repository for a portfolio application built using React and Typescript, and hosted using Gitlab pages. This is built on top of the [arifszn/gitprofile](https://github.com/arifszn/gitprofile/) repo created by [Ariful Alam](https://github.com/arifszn).

To visit the hosted version, without using dev tools, click [here.](https://portfolio.akshayc.dev/) To run this project follow these steps:

- clone the repo using `git clone`
- navigate to folder `portfolio`
- run `npm install` to instal node modules
- run `npm run dev` to start web app
